CREATE TABLE orders(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    number int NOT NULL ,
    product_id bigint NOT NULL
)
