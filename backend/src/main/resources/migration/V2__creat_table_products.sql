CREATE TABLE products
(
    id    bigint PRIMARY KEY AUTO_INCREMENT,
    name  varchar(20)  NOT NULL,
    price int       NOT NULL,
    unit  varchar(10)  NOT NULL,
    url   varchar(20000) NOT NULL
)
