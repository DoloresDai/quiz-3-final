package com.twuc.web.quizbackend.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//TODO: 太感动了，终于有人给我写@Repository了
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
