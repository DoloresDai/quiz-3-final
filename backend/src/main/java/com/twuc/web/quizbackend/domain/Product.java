package com.twuc.web.quizbackend.domain;
import java.net.URL;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;

@Entity
//TODO: 需要指定name吗？
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,unique = true)
    private String name;

    @Column(nullable = false)
    //TODO: 数据类型为int，为啥还要写NumberFormat？
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private int price;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private URL url;

    public Product() {
    }

    public Product(String name, int price, String unit, URL url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public URL getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}
