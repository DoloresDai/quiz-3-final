package com.twuc.web.quizbackend.service;

import com.twuc.web.quizbackend.contracts.ProductsRequest;
import com.twuc.web.quizbackend.domain.ProductRepository;
import com.twuc.web.quizbackend.domain.Product;
import com.twuc.web.quizbackend.exception.ProductNameRepeatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    //TODO: field injection not recommended
    @Autowired
    ProductRepository repository;
    //TODO: field injection not recommended
    @Autowired
    OrderService orderService;

    //TODO: 传Product而不是ProductsRequest
    public Product creatProduct(ProductsRequest request) throws ProductNameRepeatException {
        Optional<Product> first = repository.findAll().stream().filter(p -> p.getName().equals(request.getName())).findFirst();
        if (first.isPresent()) {
            throw new ProductNameRepeatException();
        }
        return new Product(request.getName(), request.getPrice(), request.getUnit(), request.getUrl());
    }


    //TODO: 为什么repository要有个getter？
    public ProductRepository getRepository() {
        return repository;
    }

    public void saveAndFlush(Product response) {
        repository.saveAndFlush(response);
        orderService.saveAndFlush(response);
    }

    public List<Product> getProducts() {
        return repository.findAll();
    }
}
