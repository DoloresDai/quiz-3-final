package com.twuc.web.quizbackend.contracts;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.net.URL;
import java.util.Objects;

public class ProductsRequest {

    //TODO: 创建product，为什么要传一个id过来？？？
    private Long id;
    @NotNull
    //TODO: 你这个min 1是为了说明不为空字符串吗？@NotEmpty?
    @Size(min = 1)
    private String name;
    @NotNull
    //TODO: 你这个min 1是为了说明不为空字符串吗？@NotEmpty?
    @Min(1)
    private int price;
    @NotNull
    private String unit;
    @NotNull
    private URL url;

    public ProductsRequest(String name, int price, String unit, URL url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public URL getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductsRequest that = (ProductsRequest) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
