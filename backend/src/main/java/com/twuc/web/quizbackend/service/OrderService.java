package com.twuc.web.quizbackend.service;

import com.twuc.web.quizbackend.domain.Order;
import com.twuc.web.quizbackend.domain.OrderRepository;
import com.twuc.web.quizbackend.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    //TODO: field injection not recommended
    @Autowired
    OrderRepository repository;

    public List<Order> getOrderDetails() {
        return repository.findAll();
    }

    void saveAndFlush(Product product) {
        repository.saveAndFlush(new Order(1, product));
    }

    public void deleteProduct(Long orderId) {
        repository.deleteById(orderId);
        repository.flush();
    }
}
