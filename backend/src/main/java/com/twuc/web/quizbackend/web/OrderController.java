package com.twuc.web.quizbackend.web;

import com.twuc.web.quizbackend.domain.Order;
import com.twuc.web.quizbackend.domain.Product;
import com.twuc.web.quizbackend.service.OrderService;
import com.twuc.web.quizbackend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = {"Location"})
public class OrderController {
    //TODO: field injection not recommended
    @Autowired
    OrderService orderService;

    //TODO: field injection not recommended
    @Autowired
    ProductService productService;

    //TODO: 不用写括号
    @GetMapping("")
    public ResponseEntity<List<Order>> getOrderDetails() throws MalformedURLException {
        initializeOrderRepository();

        List<Order> orderDetails = orderService.getOrderDetails();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(orderDetails);
    }

    private void initializeOrderRepository() throws MalformedURLException {
        //TODO: 使用data migration而不是hard code
        productService.saveAndFlush(new Product("cola1", 2, "bottle", new URL("https://dwz.cn/SjqgflzP")));
        productService.saveAndFlush(new Product("cola2", 32, "bottle", new URL("https://dwz.cn/SjqgflzP")));
        productService.saveAndFlush(new Product("cola3", 9, "bottle", new URL("https://dwz.cn/SjqgflzP")));
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<List<Order>> deleteProducts(@PathVariable Long orderId) throws MalformedURLException {
        initializeOrderRepository();
        orderService.deleteProduct(orderId);
        List<Order> orderDetails = orderService.getOrderDetails();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(orderDetails);
    }
}
