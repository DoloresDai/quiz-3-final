package com.twuc.web.quizbackend.web;

import com.twuc.web.quizbackend.contracts.ProductsRequest;
import com.twuc.web.quizbackend.domain.Product;
import com.twuc.web.quizbackend.exception.ProductNameRepeatException;
import com.twuc.web.quizbackend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = {"Location"})
public class ProductController {
    //TODO: field injection not recommended
    @Autowired
    ProductService productService;

    @PostMapping("products")
    public ResponseEntity<Product> saveProducts(@RequestBody @Valid ProductsRequest request) throws ProductNameRepeatException {
        //TODO: creatProduct里面不做save吗？？？
        Product response = productService.creatProduct(request);

        productService.saveAndFlush(response);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Location", "http://localhost:8080/api/products/" + response.getId())
                .build();
    }

    @GetMapping("products")
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> products = productService.getProducts();

        return ResponseEntity.ok().body(products);
    }

    @ExceptionHandler(ProductNameRepeatException.class)
    public ResponseEntity<HttpStatus> handleProductNameIsRepeatable() {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }


}
