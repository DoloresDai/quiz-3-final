package com.twuc.web.quizbackend.contracts;

//TODO: unused import
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class OrderRequest {
    //TODO: 创建product，为什么要传一个id过来？？？
    private Long id;
    @NotNull
    //TODO: 你这个min 1是为了说明不为空字符串吗？@NotEmpty?
    @Size(min = 1)
    private int number;
    @NotNull
    private ProductsRequest product;


    public OrderRequest() {
    }

    public OrderRequest(ProductsRequest product) {
        //TODO: 这个为啥给了1
        this.number = 1;
        this.product = product;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ProductsRequest getProduct() {
        return product;
    }

    public void setProduct(ProductsRequest product) {
        this.product = product;
    }
}
