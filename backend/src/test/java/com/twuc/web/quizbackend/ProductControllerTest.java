package com.twuc.web.quizbackend;

import com.twuc.web.quizbackend.contracts.ProductsRequest;
import com.twuc.web.quizbackend.domain.Product;
import com.twuc.web.quizbackend.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductControllerTest extends ApiTestBase {
    @Autowired
    ProductService productService;
    private ProductsRequest productsRequest = new ProductsRequest("cola", 3, "bottle", new URL("https://dwz.cn/SjqgflzP"));

    ProductControllerTest() throws MalformedURLException {
    }

    @Test
    void should_return_location_when_add_new_products_successfully() throws Exception {

        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", "http://localhost:8080/api/products/1"));
    }

    @Test
    void should_return_406_when_product_name_is_repeatable() throws Exception {

        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", "http://localhost:8080/api/products/1"));


        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void should_return_400_when_name_is_null() throws Exception {
        productsRequest.setName(null);
        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_price_is_0() throws Exception {
        productsRequest.setPrice(0);
        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_unit_is_null() throws Exception {
        productsRequest.setUnit(null);
        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_url_is_null() throws Exception {
        ProductsRequest productsRequest = this.productsRequest;
        productsRequest.setUrl(null);
        getMockMvc().perform(post("/api/products")
                .content(mapper.writeValueAsString(productsRequest))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_get_products() throws Exception {
        productService.saveAndFlush(new Product("cola1", 3, "bottle", new URL("https://dwz.cn/SjqgflzP")));
        productService.saveAndFlush(new Product("cola2", 3, "bottle", new URL("https://dwz.cn/SjqgflzP")));
        productService.saveAndFlush(new Product("cola3", 3, "bottle", new URL("https://dwz.cn/SjqgflzP")));

        List<Product> products = productService.getProducts();

        getMockMvc().perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

        assertEquals(3, products.size());
        assertEquals("cola1", products.get(0).getName());
    }
}

