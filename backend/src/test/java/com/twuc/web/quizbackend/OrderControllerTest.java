package com.twuc.web.quizbackend;

import com.twuc.web.quizbackend.contracts.OrderRequest;
import com.twuc.web.quizbackend.contracts.ProductsRequest;
import com.twuc.web.quizbackend.service.OrderService;
import com.twuc.web.quizbackend.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OrderControllerTest extends ApiTestBase {
    @Autowired
    ProductService productService;
    @Autowired
    OrderService orderService;

    @Test
    void should_return_all_product_details_when_successfully() throws Exception {
        List<OrderRequest> orderDetails = getAllOrderDetails();

        getMockMvc().perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(content().string(mapper.writeValueAsString(orderDetails)));
    }

    private List<OrderRequest> getAllOrderDetails() throws MalformedURLException {
        ProductsRequest response1 = new ProductsRequest("cola1", 2, "bottle", new URL("https://dwz.cn/SjqgflzP"));
        ProductsRequest response2 = new ProductsRequest("cola2", 32, "bottle", new URL("https://dwz.cn/SjqgflzP"));
        ProductsRequest response3 = new ProductsRequest("cola3", 9, "bottle", new URL("https://dwz.cn/SjqgflzP"));
        response1.setId(1L);
        response2.setId(2L);
        response3.setId(3L);

        OrderRequest orderRequest1 = new OrderRequest(response1);
        OrderRequest orderRequest2 = new OrderRequest(response2);
        OrderRequest orderRequest3 = new OrderRequest(response3);
        orderRequest1.setId(1L);
        orderRequest2.setId(2L);
        orderRequest3.setId(3L);

        return Arrays.asList(orderRequest1, orderRequest2, orderRequest3);
    }

    private List<OrderRequest> getDeleteOrderDetails() throws MalformedURLException {
        ProductsRequest response2 = new ProductsRequest("cola2", 32, "bottle", new URL("https://dwz.cn/SjqgflzP"));
        ProductsRequest response3 = new ProductsRequest("cola3", 9, "bottle", new URL("https://dwz.cn/SjqgflzP"));
        response2.setId(2L);
        response3.setId(3L);

        OrderRequest orderRequest2 = new OrderRequest(response2);
        OrderRequest orderRequest3 = new OrderRequest(response3);
        orderRequest2.setId(2L);
        orderRequest3.setId(3L);

        return Arrays.asList(orderRequest2, orderRequest3);
    }
    @Test
    void should_return_200_when_delete_product() throws Exception {
        getMockMvc().perform(delete(("/api/orders/1")))
                .andExpect(status().isOk());
    }

    @Test
    void should_decrease_one_product_when_delete_successfully() throws Exception {
        getMockMvc().perform(delete(("/api/orders/1")))
                .andExpect(content().string(mapper.writeValueAsString(getDeleteOrderDetails())));

    }
}
