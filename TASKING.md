初始化项目
- backend
    - 配置。。。
    - 启动docker，能使用数据库
    - 数据库的初始化--flyway
- frontend
    - 配置。。。
- .gitignore 要分别将前后端已经搭好的脚手架中的.gitignore文件中的路径进行修改
后转移到quiz-3-final中，同时删除各自的.gitignore文件

##backend
1. 数据库有两张表
    1. prodcuts(id name price unit)
    2. details(id number product_id)
    3. details中的product_id与products中的id是多对一的关系
    4. 数据库的填充用英文
2. 设计api
    1. 商城页面：“/api/products”
       1. 点击add按钮进入订单页面（“api/orders”），且添加新商品
       2. 点击add按钮在尚未接收到响应时，按钮disabled，接收后才可继续点击
    2. 订单页面：“/api/orders”
       1. 显示商品列表
       
       2. 每个商品后有删除按钮，点击重新渲染页面，删除商品不存在。删除失败，弹框中提示“商品删除失败，请稍后再试”
       
          delete("/api/orders/{orderId}")
       
       3. 无订单时，“暂无订单，返回商城页面继续购买”
       
       4. 点击“商城页面”返回到商城页
    3. 添加商品：“/api/products/”
       1. post
       2. header(new Product())
       3. 成功返回body location
       4. 错误post：返回400
       5. 提交成功：返回到商城页，可查看新创建的商品信息
       6. 字段均不能为空，价格为数字。若不满足格式，“提交”按钮不可点击
       7. 若商品名称已经存在，点击“提交”后，弹框中提示“商品名称已存在，请输入新的商品名称”
