import {combineReducers} from "redux";
import productReducer from "../product/reducer/productReducer";
import orderReducer from "../Order/reducer/orderReducer";

const reducers = ()=>combineReducers({
    product:productReducer,
    order:orderReducer,
});
export default reducers();
