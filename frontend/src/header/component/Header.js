import React from 'react';
import {Link} from "react-router-dom";

const Header = () => (
    <div className="header">
        <ul>
            <li className="header-li">
                <Link to={"/"}>
                    <span>商城</span>
                </Link>
            </li>
            <li className="header-li">
                <Link to={"/orders"}>
                    <span>订单</span>
                </Link>
            </li>
            <li className="header-li">
                <Link to={"/products"}>
                    <span>添加商品</span>
                </Link>
            </li>
        </ul>
    </div>
);

export default Header;
