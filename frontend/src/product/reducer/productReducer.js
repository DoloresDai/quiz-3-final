//TODO: product 是个数组？？
const initial = {
    product: {}
};
export default (state = initial, action) => {
    switch (action.type) {
        case "PRODUCTS":
            return {
                ...state,
                product: action.product,
            };
        default:return state;
    }
}
