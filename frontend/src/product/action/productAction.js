export const getProducts = () => (dispatch) => {
    fetch("http://localhost:8080/api/products")
        .then(res => res.json())
        .then(result => {
            dispatch({
                type: "PRODUCTS",
                product: result
            })
        })
};
