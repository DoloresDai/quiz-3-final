import React, {Component} from 'react';
import {getProducts} from "../action/productAction";
import {MdAdd} from "react-icons/md";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class Product extends Component {
    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        let {product} = this.props.product;
        product = Array.from(product);

        return (
            <div className="products">
                <ul className="products-ul">
                    {
                        product.map((value => {
                            return (
                                <li className="product-li" key={"li-" + value.id}>
                                    <img className="product-img" src={value.url}/>
                                    <span className="product-name">{value.name}</span>
                                    <span className="product-price">{value.price + "元/" + value.unit}</span>
                                    <MdAdd/>
                                </li>
                            )
                        }))
                    }
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    product: state.product,
});
const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Product);
