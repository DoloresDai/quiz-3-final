import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import Header from "./header/component/Header";
import Product from "./product/component/Product";
import {Route, Switch} from "react-router";

class App extends Component {
    render() {
        return (
            <div className='App'>
                <Router>
                    <Header/>
                    <Switch>
                        <Route path="/" component={Product}></Route>
                        {/*<Route path="/orders" component={Order}></Route>*/}
                        {/*<Route path="/add" component={Add}></Route>*/}
                    </Switch>
                </Router>
            </div>
        )
    }
}

export default App;
