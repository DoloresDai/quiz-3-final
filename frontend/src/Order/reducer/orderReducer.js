const initial = {
    order: {}
};

export default (state = initial, action) => {
    switch (action.type) {
        case "ORDER":
            return {
                ...state,
                order: state.order,
            }
                ;
        default:
            return state;
    }
}
