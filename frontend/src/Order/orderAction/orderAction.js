const getOrders = () => (dispatch) => {
    fetch("http://localhost:8080/api/orders")
        .then(res => res.json())
        .then(result => {
            dispatch({
                type: "ORDERS",
                order: result
            })
        })
};
