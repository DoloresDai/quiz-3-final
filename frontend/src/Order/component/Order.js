import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {MdDelete} from "react-icons/md";


class Order extends Component {
    componentDidMount() {
        this.props.getOrders();
    }

    render() {
        const {order} = this.props;
        return (
            <div className="order">
                <table>
                    <tr>
                        <th>名字</th>
                        <th>单价</th>
                        <th>数量</th>
                        <th>单位</th>
                        <th>操作</th>
                    </tr>

                    {
                        order.map(order => {
                            return (
                                <tr key={"tr-" + order.id}>
                                    <td>{order.name}</td>
                                    <td>{order.price}</td>
                                    <td>{order.number}</td>
                                    <td>{order.unit}</td>
                                    <td><MdDelete/></td>
                                </tr>
                            )
                        })
                    }
                </table>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    order: state.order,
});
const mapDispatchToProps = dispatch => bindActionCreators({
    getOrders,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);
